from flask import Flask, jsonify, request, abort
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file"""  """
data = json.load(open('coe332.json', 'r'))

@app.route('/')
def coe332():
    return jsonify(data)      

@app.route('/instructors')
def get_instructors(): 
    return jsonify(data['instructors'])  

@app.route('/instructors/<int:number>')
def get_instructor(number): 
        if number < len(data['instructors']): 
                return jsonify(data['instructors'][number])
        else : 
                abort(404)

@app.route('/meeting')
def get_meeting():
    return jsonify(data['meeting'])

@app.route('/meeting/<string:type>')
def get_meeting_type(type):
        return jsonify(data['meeting'][type]) 
        

@app.route('/assignments', methods = ['POST','GET'])
def post_assignment(): 
        if request.method == 'POST': 
                request_data = request.get_json()       
                data["assignments"].append(request_data)
                return "Assignment entered."
        else: 
                return jsonify(data["assignments"]) 

@app.route('/assignments/<int:number>') 
def get_assignment(number): 
        if number < len(data['instructors']):    
                return jsonify(data['assignments'][number])
        else: 
                abort(404) 

@app.route('/assignments/<int:number>/url') 
def get_assignments_url(number): 
        if number < len(data['instructors']):    
                return jsonify(data['assignments'][number]['url']) 
        else: 
                abort(404) 